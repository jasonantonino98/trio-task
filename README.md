# Trio-task
<!-- This is a Flask application that is set up and configured to work with a database and nginx. Write a docker-compose.yaml that will bring all these services up and allow the app to run on port `80`. -->

## Contents
* Virtual Private Cloud (VPC)
* Subnets
  * Public Subnet
  * Private Subnet
* Route Tables
  * Public Route Table
  * Private Route Table
* Security Groups
  * Public Security Group
  * Private Security Group
* Internet Gateway
* EC2 Instance
* RDS Instance
* Docker


## Virtual Private Cloud (VPC)
For this project, I have created a custom VPC using the AWS VPC service. During the creation of this, I have assigned the VPC with a name of awsChallengeVPC and an IPv4 CIDR of 10.0.0.0/16.
![VPC Information](images/vpc_info.png)
![VPC CIDR](images/vpc_cidr.png)


## Subnets
For this project, the AWS infrastructure must contain 3 subnets, 1 of which is public and the remaining subnets set as private.

### Public Subnet
In order to make a subnet public, an internet gateway must be attached to the VPC and this would then need to be added to the route table of the public subnet. Therefore, I have added the Internet Gateway to the route table attached to the public subnet which has a destination of 0.0.0.0/0, which is shown in the image below:
![Public Subnet Route Table](images/public_subnet_route_table.png)

In addition to this, I have set up the public subnet so that it is within the VPC I have created (awsChallengeVPC) and I have selected the availability zone eu-west-1a for this public subnet. All of these details are shown in the image below:
![Public Subnet Information](images/public_subnet_info.png)

### Private Subnet
By default, newly created subnets are private. This means that for this project, I have used the default route table which was automatically created along with the VPC, as shown in the image below:
![Private Subnet Route Table](images/private_subnet_route_table.png)

To add to this, I have also placed the two private subnets within the same VPC as the public subnet (awsChallengeVPC) and for the private subnet which will contain the RDS instance, I have placed it within the eu-west-1a availability zone, as shown below:
![Private Subnet Information](images/private_subnet_info.png)

As for the other private subnet, I have placed this within the eu-west-1b availability zone.


## Route Tables
Route tables are used in order to determine where the network traffic is directed across the AWS infrastructure. This can include traffic from the subnet or within gateways, such as the Internet Gateway. For this project, there are a total of two route tables which was implemented.

### Public Route Table
A public route table is typically associated with a public subnet as it allows traffic from the internet, usually through the Internet Gateway. Due to this, I have created a new route table named "awsChallengePublicRoute" which has been added to the same VPC as the other services (awsChallengeVPC). Once this has been created, I have then associated the route table to the public subnet, as shown by "Explicit subnet associations" in the image below:
![Public Route Table](images/public_route_table_info.png)

And these are the routes within this public route:
![Public Route Routes](images/public_route_routes.png)

### Private Route Table
For this project, the private subnets use the default route table which was generated alongside the VPC, which is named "awsChallengeVPCRoute". This can be seen in the image below, where there are two explicit subnet associations:
![Private Route Table](images/private_route_table_info.png)

As shown in the image below, this route table only contains one route, which is the local route. This route allow subnets within the same VPC to communicate with each other. Due to the fact that there are no other routes within this route table, it means that this is a private route table.
![Private Route Table Routes](images/private_route_routes.png)


## Security Groups
Security groups are a form of firewall that work at an instance level. This means that it sets rules which only allow specified traffic to communicate with the instances within the given security group. For this project, I have created two security groups, one of which is used for the EC2 instanace located within the public subnet and the other security group is designated within the private subnet containing the RDS instance.

### Public Security Group
For the public security group, there are a total of three inbound rules, as shown in the image below. These rules allow SSH connection as well as connection to ports 80 and 5000, which will be used when accessing the web application. This security group was also added to the same VPC.
![Public Security Group Inbound Rules](images/public_security_group_rules.png)

### Private Security Group
For the private security group, there is only one inbound rule, as shown in the image below. This rule only allow MySQL/Aurora traffic that are coming from instances that have access to the public security group . This security group was also added to the same VPC.
![Private Security Group Inbound Rules](images/private_security_group_rules.png)


## Internet Gateway
The internet gateway is a resource that is attached to a VPC and this gives the VPC the ability to communicate to the Internet. For this project, I have created a new Internet gateway named "awsChallengeInternetGateway" and this has been attached to the VPC named "awsChallengeVPC". This can be confirmed by the image shown below:
![Internet Gateway Information](images/internet_gateway_info.png)


## EC2 Instance
For this project, the EC2 instance has the role of running the web application. Therefore, this instance has been placed within the public subnet as it will require access to the internet and the public subnet has a route leading to the Internet Gateway. In addition to this, the EC2 instance has been placed within the custom VPC created earlier. Both of these are shown in the image below:
![EC2 instance VPC and Subnet](images/ec2_vpc_and_subnet.png)

Another configuration that has been done was to place the EC2 instance within the security group named "awsChallengePublicSecurityGroup". In doing this, the EC2 instance will have the ability to SSH but also connect to ports 80 and 5000. This is shown in the image below:
![EC2 instance Security Group](images/ec2_security_group.png)


## RDS Instance
For this project, the RDS instance that was created uses the MySQL database running on a t2.micro instance. This RDS instance has been given the name "awsChallenge" and within this instance, a database has been generated which is named "awschallenge".
![RDS instance VPC and Security Group](images/rds_info.png)

As shown in the image below, the RDS instance created has been placed within the "awsChallengeVPC" and has been given the security group called "awsChallengePrivateSecurityGroup", which only allow MySQL inbound traffic. The instance is also not publicly accessible to ensure the security of the database.
![RDS instance VPC and Security Group](images/rds_vpc_and_sg.png)


## Docker
The image below shows an sh file which was used in order to create a:
* Docker netowrk
* Volume
* flask-app image
* database image
* Run flask-app container
* Run NGINX container

Running this sh file will create the Docker containers required which in turn will run the flask application.
![Sh file to create Docker containers](images/docker_container.png)

## Known Issues
1. The Database URI has not been hidden using environment variables which could result in a breach of the RDS instance. To prevent this from happening, I have shut down and deleted the RDS instance.

## Future Work
1. To ensure high availability and resiliency, the services should be placed within different availability zones. This means that in the event of an outage of an availability zone, only the services within that zone will be affected. In addition to this, future work should have multiple instances of a service at different availability zones so that if one zone goes down, the service affected can be quickly spun up in a different zone.

2. To improve the security of the AWS infrastructure, future work could include the use of IAM users, groups and roles. To support this, the CloudTrail service could also be used. This means that any activity that are being done within the AWS infrastructure can be monitored, which would greatly increase the security.

3. To improve the reusability and the repeatability, the whole AWS infrastructure could be created within CloudFormation. This service will allow the creation of resources within one stack. The benefit of this is that this will create a "single source of truth" which means that everyone working on the same project are able to work with the same codebase which can help reduce compatibility issues. In addition to this, using CloudFormation means that testing and deployment can be automated and done more efficiently as the creation of the infrastructure can be done more easily.